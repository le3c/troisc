<?php 

/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php	 	 language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php	 	 language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php	 	 language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php	 	 bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php	 	 wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php	 	 bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php	 	 echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>

	<link href='http://fonts.googleapis.com/css?family=Mouse+Memoirs' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />

	<?php

		function in_string($findme, $mystring)
		{
			$pos = strpos($mystring, $findme);
			
			if ($pos !== false) return true;
			else return false;
		}


		if( !in_string("forum", $_SERVER[REQUEST_URI]) ) echo "<script src='http://code.jquery.com/jquery-2.0.3.min.js'></script>";

	?>


	<script>

		$(document).ready(function()
		{
			$(".eventHead, .eventname").click(function()
			{
				//$('.leTexte').slideUp();
                //window.alert(getComputedStyle($(this).siblings('.leTexte'), null).display);
                //var cetitre = $(this).siblings('.leTexte');
                //window.alert(getComputedStyle(cetitre, null).display);
				$(this).siblings('.leTexte').slideToggle();
			});
		});

	</script>

		<?
			$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
			$thumb_url = $thumb_url_array[0];
			if(empty($thumb_url)) $thumb_url = "http://cafeculturelcitoyen.org/wp-content/uploads/2015/02/unnamed-285x300.png";
			echo " <meta property='og:image' content='$thumb_url' /> ";
			// echo " <meta property='og:description' content=\" ". the_content() ." \" /> ";
		?>


	

</head>

<body <?php	 	 body_class(); ?>>


	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">

			<div style="position: absolute; width: 100%; height: 100%; left: 0; top: 0; "> <!-- background: rgba(78, 17, 17, 0.29); -->
			</div>

			<table style="border: none; position: relative; float: left; width: 100%; height: 230px; margin: 0 ">
				<tr style="vertical-align: top;">
					<td style="width: 5em;">
						<a href="<? echo get_site_url(); ?>" title="Accueil">
							<img src="<? echo get_site_url(); ?>/wp-content/uploads/bulle_blanche.png" id="bulleBlanche">
						</a>
					</td>
					<td> <? get_sidebar('main'); ?> </td>
				</tr>
			</table>

			
			<a class="home-link" href="<?php	 	 echo esc_url( home_url( '/' ) ); ?>" title="<?php	 	 echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<h1 class="site-title"><?php	 	 bloginfo( 'name' ); ?></h1>
				<h2 class="site-description"><?php	 	 bloginfo( 'description' ); ?></h2>
			</a>
			

			<div id="navbar" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<h3 class="menu-toggle"><?php	 	 _e( 'Menu', 'twentythirteen' ); ?></h3>
					<a class="screen-reader-text skip-link" href="#content" title="<?php	 	 esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php	 	 _e( 'Skip to content', 'twentythirteen' ); ?></a>
					<?php	 	 wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
					<?php	 	 get_search_form(); ?>
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->

		</header><!-- #masthead -->

		<? dynamic_sidebar('Alert Area'); ?>

<?  //echo "<p>test</p>" . get_the_content('Read more'); ?>



		<div id="main" class="site-main">
