# Troisc

## Description

Un thème pour Wordpress, développé pour le site cafeculturelcitoyen.org

## Installation

À décompresser dans le dossier wp-content/themes/troisc, puis dans l'interface d'admin du site choisir le thème "troisc"

## Licence

GPL v3
